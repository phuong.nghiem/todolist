import React from "react";
import { useDispatch } from "react-redux";
import * as ActionType from "./../../redux/reducers/ToDoList/constant";

interface Props {
  getKeyWord: (e:any) => void
}

const Search: React.FC<Props> = (props: any) => {
  const dispatch = useDispatch();

  const handleOnChangeSearch = (e: any) => {
    props.getKeyWord(e.target.value);
    dispatch({
      type: ActionType.SEARCH_TASK,
      payload: e.target.value
    })
  }

  return (
    <div className="input-group">
      <input
        type="text"
        className="form-control"
        placeholder="Search Task"
        onChange={handleOnChangeSearch}
      />
      <div className="input-group-append">
        <button className="btn btn-secondary" type="button">
          <i className="fa fa-search" />
        </button>
      </div>
    </div>
  );
};

export default Search;
