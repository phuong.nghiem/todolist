export const GET_TASK_API = "GET_TASK_API";
export const SET_TASK_API = "SET_TASK_API";
export const ADD_TASK_API = "ADD_TASK_API";
export const DELETE_TASK_API = "DELETE_TASK_API";
export const EDIT_TASK_API = "EDIT_TASK_API";
export const SEARCH_TASK = "SEARCH_TASK";
export const SET_KEY_WORDS = "SET_KEY_WORDS";


export const SET_EDIT_TASK = "SET_EDIT_TASK";

export const DISPLAY_LOADING = "DISPLAY_LOADING";
export const HIDE_LOADING = "HIDE_LOADING";